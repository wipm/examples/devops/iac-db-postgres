#!/bin/bash
set -e
SCRIPT_DIR=$(dirname $0)


if [ "$(psql -tAc "SELECT 1 FROM pg_database WHERE datname='$DB_NAME'" )" = "1" ]; then 
    echo "Database already exists [$DB_NAME]"
else
    echo "Creating Database [$DB_NAME]"
    psql -d postgres --set=DB_USER=${DB_USER} --set=DB_NAME=${DB_NAME} -f ${SCRIPT_DIR}/001-CreateDatabase.sql
fi

if [ "$(psql -tAc "SELECT 1 FROM pg_roles WHERE rolname='$DB_USER'")" =  "1" ]; then
    echo "Role already exists [$DB_USER]"
else
    echo "Creating Role [$DB_USER]" 
    psql -d postgres --set=DB_USER=${DB_USER} --set=DB_PASSWORD=${DB_PASSWORD} --set=DB_NAME=${DB_NAME} -f ${SCRIPT_DIR}/002-CreateRole.sql
fi

echo "Creating schema [Product]"
psql -d ${DB_NAME} --set=DB_USER=${DB_USER} -f ${SCRIPT_DIR}/003-CreateSchema.sql

echo "Creating tables in schema[Product]"
psql -d ${DB_NAME} -f ${SCRIPT_DIR}/004-CreateTable.sql

#if [ "$(psql -tAc "SELECT 1 FROM pg_roles WHERE rolname='$DB_USER'")" = '1']; then
#    echo "Role already exists"
#else
#    psql -d postgres --set=DB_USER=${DB_USER} --set=DB_PASSWORD=${DB_PASSWORD} --set=DB_NAME=${DB_NAME} -f ${SCRIPT_DIR}/002-CreateRole.sql
#fi

#echo "Creating schema"
#psql -d ${DB_NAME} --set=DB_USER=${DB_USER} --set=DB_PASSWORD=${DB_PASSWORD} -f ${SCRIPT_DIR}/003-CreateSchema.sql

#echo "Create tables"
#psql -d ${DB_NAME} --set=DB_USER=${DB_USER} --set=DB_PASSWORD=${DB_PASSWORD} -f ${SCRIPT_DIR}/004-CreateTables.sql


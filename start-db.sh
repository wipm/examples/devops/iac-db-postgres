#!/bin/bash

set -e
SCRIPT_DIR=$(dirname $0)

export DB_NAME="context"
export DB_USER="dimensions"
export DB_PASSWORD="userpassword"
export PGHOST="localhost"
export PGUSER="postgres"
export PGPASSWORD="password"

docker run \
    --name postgres \
    --rm \
    -e POSTGRES_PASSWORD=$PGPASSWORD \
    -d \
    -p 5432:5432 \
    postgres:15

# Wait for connection to be established to db
if ! timeout 120s bash -c 'until pg_isready; do sleep 1; done'; then
    exit 1
fi

echo "Applying sql scripts"
./deployment/sql/create-db.sh
